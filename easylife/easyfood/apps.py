from django.apps import AppConfig


class EasyfoodConfig(AppConfig):
    name = 'easyfood'
