from django.contrib import admin

# Register your models here.


from .models import Product, Category, Nutritionist, DayMenu, Comment, Profile


class CommentAdmin(admin.ModelAdmin):
    list_display = ('name', 'body', 'product', 'created_on', 'active')
    list_filter = ('active', 'created_on')
    search_fields = ('name', 'email', 'body')
    actions = ['approve_comments']


    def approve_comments(self, request, queryset):
        queryset.update(active=True)


admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Profile)
admin.site.register(Nutritionist)
admin.site.register(DayMenu)
admin.site.register(Comment)
