"""easylife URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""


from django.urls import path
from .views import AboutView, HomePageView, ProductListView, ProductView, ProductDetailView, ProductUpdateView, \
    ProductDeleteView, DayMenuListView, DayMenuDetailView, DayMenuUpdateView, \
    DayMenuDeleteView
from . import views

urlpatterns = [
    path('about', AboutView.as_view()),
    path('', HomePageView.as_view(), name='home'),
    path('register/', views.user_register, name='user_register'),
    path('product/', ProductView.as_view(), name='product'),
    path('product/list/', ProductListView.as_view(), name="product_list"),
    path('detail/<int:pk>', ProductDetailView.as_view(), name='product_detail'),
    path('update/<int:pk>', ProductUpdateView.as_view(), name='product_update'),
    path('delete/<int:pk>', ProductDeleteView.as_view(), name='product_delete'),
    path('day_menu/', DayMenuListView.as_view(), name="day_menu_list"),
    path('day_menu/detail/<int:pk>', DayMenuDetailView.as_view(), name='day_menu_detail'),
    path('day_menu/update/<int:pk>', DayMenuUpdateView.as_view(), name='day_menu_update'),
    path('day_menu/delete/<int:pk>', DayMenuDeleteView.as_view(), name='day_menu_delete'),
    # path('product/list/nutritionist', NutritionistView.as_view(), name="nutritionist"),
    path('product/list/nutritionist', views.post_detail, name='nutritionist'),
    # path('product/list/nutritionist', views.PostList.as_view(), name="post_list"),
]

