from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy, reverse
from django.views.generic import TemplateView, ListView, DetailView, UpdateView, DeleteView

from .forms import RegisterForm, CommentForm
from .models import Product, DayMenu, User, Comment


class HomePageView (TemplateView):
    template_name = "base.html"


class ProductView (TemplateView):
    template_name = "product.html"


# class NutritionistView (TemplateView):
#     template_name = "nutritionist.html"


class AboutView (TemplateView):
    template_name = "about.html"


def user_register(request):
    # if this is a POST request we need to process the form data
    template = 'registration/register.html'

    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = RegisterForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            if User.objects.filter(username=form.cleaned_data['username']).exists():
                return render(request, template, {
                    'form': form,
                    'error_message': 'Username already exists.'
                })
            elif User.objects.filter(email=form.cleaned_data['email']).exists():
                return render(request, template, {
                    'form': form,
                    'error_message': 'Email already exists.'
                })
            elif form.cleaned_data['password'] != form.cleaned_data['password_repeat']:
                return render(request, template, {
                    'form': form,
                    'error_message': 'Passwords do not match.'
                })
            else:
                # Create the user:
                user = User.objects.create_user(
                    form.cleaned_data['username'],
                    form.cleaned_data['email'],
                    form.cleaned_data['password']
                )
                user.first_name = form.cleaned_data['first_name']
                user.last_name = form.cleaned_data['last_name']
                user.phone_number = form.cleaned_data['phone_number']
                user.save()

                user = authenticate(request, username=form.cleaned_data['username'], password=form.cleaned_data['password'])
                # Login the user
                login(request, user)

                # redirect to accounts page:
                return HttpResponseRedirect('/')

    # No post data availabe, let's just show the page.
    else:
        form = RegisterForm()

    return render(request, template, {'form': form})


def login_user(request):
    form = AuthenticationForm()
    return render(request=request,
                   template_name="login.html",
                   context={"form": form})


def logout_view(request):
    logout(request)
    return redirect('')


class ProductListView (ListView):
    model = Product
    template_name = "product_list.html"
    context_object_name = "products"


class ProductDetailView (DetailView):
    model = Product
    template_name = "product_detail.html"
    context_object_name = "product"


class ProductUpdateView (UpdateView):
    model = Product
    template_name = "product_update.html"
    context_object_name = "product"
    fields = '__all__'
    success_url = reverse_lazy ('product_list')

    def post(self, request, pk):
        product = Product.objects.filter(pk=pk)
        items = request.POST.dict()
        del items['csrfmiddlewaretoken']
        product.update(**items)
        return HttpResponseRedirect(reverse('product_detail', args=(pk,)))


class ProductDeleteView (DeleteView):
    model = Product
    template_name = "product_delete.html"
    context_object_name = "product"
    fields = '__all__'
    success_url = reverse_lazy('product_list')

    def post(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('product_list'))
        else:
            return super().post(request, *args, **kwargs)


class DayMenuListView(ListView):
    model = DayMenu
    template_name = "day_menu_list.html"
    context_object_name = "day_menu"


class DayMenuUpdateView(UpdateView):
    model = DayMenu
    template_name = "day_menu_update.html"
    context_object_name = "day_menu"
    fields = '__all__'
    success_url = reverse_lazy('day_menu_list')

    def post(self, request, pk):
        product = Product.objects.filter(pk=pk)
        items = request.POST.dict()
        del items['csrfmiddlewaretoken']
        product.update(**items)
        return HttpResponseRedirect(reverse('day_menu_detail', args=(pk,)))


class DayMenuDetailView(DetailView):
    model = DayMenu
    template_name = "day_menu_detail.html"
    context_object_name = "day_menu"


class DayMenuDeleteView(DeleteView):
    model = DayMenu
    template_name = "day_menu_delete.html"
    context_object_name = "day_menu"
    fields = '__all__'
    success_url = reverse_lazy('day_menu_list')


    def post(self, request, *args, **kwargs):
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('day_menu_list'))
        else:
            return super().post(request, *args, **kwargs)



# class PostList(generic.ListView):
#     queryset = Post.objects.filter(status=1).order_by("-created_on")
#     template_name = "nutritionist.html"
#     paginate_by = 3


def post_detail(request):
    template_name = 'nutritionist.html'
    product = get_object_or_404(Product, pk=1)
    comments = Comment.objects.filter(active=True).order_by("-created_on")
    new_comment = None
    # Comment posted
    if request.method == 'POST':
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():

            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.product = product
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()

    return render(request, template_name, {'product': product,
                                           'comments': comments,
                                           'new_comment': new_comment,
                                           'comment_form': comment_form})