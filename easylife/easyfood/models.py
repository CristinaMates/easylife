from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name


class Product(models.Model):
    # objects = models
    name = models.CharField(max_length=50)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    description = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=4, decimal_places=2)
    image = models.ImageField(upload_to='static', default='image.jpg')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "your Program"


class Nutritionist(models.Model):
    name = models.CharField(max_length=50)
    appointment = models.BooleanField()
    tests = models.BooleanField()
    consultation = models.BooleanField()

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    nutritionist = models.ForeignKey(Nutritionist, on_delete=models.CASCADE)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    address = models.CharField(max_length=30, default="")
    health_situation = models.CharField(max_length=30, default="")
    phone = models.CharField(max_length=20, default="")

    def __str__(self):
        return self.user.name


class DayMenu(models.Model):
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    day_menu = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=4, decimal_places=2)
    image = models.ImageField(upload_to='static', default='image.jpg')

    def __str__(self):
        return self.day_menu

    class Meta:
        verbose_name_plural = "Healthy Day"


class Comment(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    name = models.CharField(max_length=80)
    email = models.EmailField()
    body = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=False)

    class Meta:
        ordering = ["created_on"]

    def __str__(self):
        return "Comment {} by {}".format(self.body, self.name)






